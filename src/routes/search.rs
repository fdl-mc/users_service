use crate::{models::user, responses::AppError, AppState};
use axum::{
    extract::{Query, State},
    http::StatusCode,
    response::IntoResponse,
    Json,
};
use sea_orm::{ColumnTrait, EntityTrait, QueryFilter};
use serde::Deserialize;
use utoipa::IntoParams;

#[derive(Deserialize, IntoParams)]
pub struct SearchQuery {
    /// Username prefix
    username: String,
}

/// Search users by query.
#[utoipa::path(
    get, path = "/search", tag = "Users",
    params(SearchQuery),
    responses(
        (status = 200, body = [User], description = "Successful fetch"),
    )
)]
pub async fn search(
    State(state): State<AppState>,
    Query(params): Query<SearchQuery>,
) -> impl IntoResponse {
    user::Entity::find()
        .filter(user::Column::Nickname.starts_with(&params.username))
        .all(&state.conn)
        .await
        .map(Json)
        .map_err(|err| {
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(AppError::new(err.to_string())),
            )
        })
}
