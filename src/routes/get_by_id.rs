use crate::{models::user, responses::AppError, AppState};
use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::IntoResponse,
    Json,
};
use sea_orm::EntityTrait;

/// Get a user by ID.
#[utoipa::path(
    get, path = "/{id}", tag = "Users",
    params(
        ("id" = i32, Path, description = "User ID")
    ),
    responses(
        (status = 200, body = User, description = "Successful fetch"),
        (status = 404, body = AppError, description = "User not found"),
    )
)]
pub async fn get_by_id(State(state): State<AppState>, Path(id): Path<i32>) -> impl IntoResponse {
    user::Entity::find_by_id(id)
        .one(&state.conn)
        .await
        .map(|e| {
            e.map(Json)
                .ok_or((StatusCode::NOT_FOUND, Json(AppError::new("User not found"))))
        })
        .map_err(|err| {
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(AppError::new(err.to_string())),
            )
        })
}
