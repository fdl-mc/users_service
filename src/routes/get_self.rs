use crate::{extractors::AuthClaims, models::user, responses::AppError, AppState};
use axum::{extract::State, http::StatusCode, response::IntoResponse, Json};
use sea_orm::EntityTrait;

/// Get self user object.
#[utoipa::path(
    get, path = "/me", tag = "Users",
    responses(
        (status = 200, body = User, description = "Successful fetch"),
        (status = 401, body = AppError, description = "Authentication failed"),
    ),
    security(("api_key" = []))
)]
pub async fn get_self(
    AuthClaims(claims): AuthClaims,
    State(state): State<AppState>,
) -> impl IntoResponse {
    user::Entity::find_by_id(claims.user_id)
        .one(&state.conn)
        .await
        .map(|e| {
            e.map(Json)
                .ok_or((StatusCode::NOT_FOUND, Json(AppError::new("User not found"))))
        })
        .map_err(|err| {
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(AppError::new(err.to_string())),
            )
        })
}
