use crate::{
    claims::Claims,
    models::{credential, user},
    responses::AppError,
    AppState,
};
use axum::{extract::State, http::StatusCode, Json};
use sea_orm::{ColumnTrait, EntityTrait, QueryFilter};
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

/// Data used in login
#[derive(Deserialize, ToSchema)]
pub struct LoginRequest {
    /// Username
    username: String,
    /// Password
    password: String,
}

#[derive(Serialize, ToSchema)]
pub struct LoginResponse {
    /// API token
    token: String,
}

/// Log into account.
#[utoipa::path(
    post, path = "/login", tag = "Auth",
    request_body = LoginRequest,
    responses(
        (status = 200, body = LoginResponse, description = "Successfully loggrd into account"),
        (status = 401, body = AppError, description = "Wrong username or password"),
    ),
)]
pub async fn login(
    State(state): State<AppState>,
    Json(data): Json<LoginRequest>,
) -> Result<Json<LoginResponse>, (StatusCode, Json<AppError>)> {
    // Find a user and related credential
    let res = user::Entity::find()
        .filter(user::Column::Nickname.eq(&*data.username))
        .find_also_related(credential::Entity)
        .one(&state.conn)
        .await
        .map_err(|e| {
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(AppError::new(e.to_string())),
            )
        })?;

    // Unwrap data
    let (user, credential) = res.ok_or((
        StatusCode::UNAUTHORIZED,
        Json(AppError::new("Wrong username or password")),
    ))?;

    // Unwrap credential
    let credential = credential.ok_or((
        StatusCode::INTERNAL_SERVER_ERROR,
        Json(AppError::new(
            "Critical error: credential not found! Please report to staff.",
        )),
    ))?;

    // Verify password
    if !credential.verify_password(&data.password) {
        return Err((
            StatusCode::UNAUTHORIZED,
            Json(AppError::new("Wrong username or password")),
        ));
    }

    // Create JWT
    let token = Claims::new(user.id)
        .to_jwt(&state.config.jwt_secret)
        .map_err(|err| {
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(AppError::new(format!(
                    "Error while maiking JWT key: {}",
                    err
                ))),
            )
        })?;

    Ok(Json(LoginResponse { token }))
}
