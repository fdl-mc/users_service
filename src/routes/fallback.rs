use axum::{response::IntoResponse, Json};

use crate::responses::AppError;

pub async fn fallback() -> impl IntoResponse {
    Json(AppError::new("Requested path not found"))
}
