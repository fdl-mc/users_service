use jsonwebtoken::{
    decode, encode, errors::Result, Algorithm::HS256, DecodingKey, EncodingKey, Header, Validation,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Claims {
    pub user_id: i32,
}

impl Claims {
    pub fn new(user_id: i32) -> Self {
        Claims { user_id }
    }

    pub fn from_jwt(token: &str, secret: &str) -> Result<Claims> {
        let mut validation = Validation::new(HS256);
        validation.required_spec_claims.remove("exp");
        validation.validate_exp = false;
        Ok(decode::<Claims>(
            token,
            &DecodingKey::from_secret(secret.as_bytes()),
            &validation,
        )?
        .claims)
    }

    pub fn to_jwt(&self, secret: &str) -> Result<String> {
        encode(
            &Header::default(),
            self,
            &EncodingKey::from_secret(secret.as_bytes()),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const JWT: &str =  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.b8u3KJdScCZM3VABdI8VOknMpuQYpblxSl-1EmDCVzQ";
    const SECRET: &str = "testsecret";

    #[test]
    fn test_to_jwt() -> Result<()> {
        assert_eq!(Claims::new(1).to_jwt(SECRET)?, JWT);
        Ok(())
    }

    #[test]
    fn test_from_jwt() -> Result<()> {
        assert_eq!(Claims::from_jwt(JWT, SECRET)?, Claims::new(1));
        Ok(())
    }
}
