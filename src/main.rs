use std::net::SocketAddr;

use axum::{
    routing::{get, post},
    Router,
};
use sea_orm::{Database, DbConn};
use sea_orm_migration::MigratorTrait;
use serde::Deserialize;
use tower_http::trace::TraceLayer;
use tracing_subscriber::prelude::*;

use crate::{
    migration::Migrator,
    openapi::ApiDoc,
    routes::{fallback, get_by_id, get_self, login, search},
};

mod claims;
mod extractors;
mod migration;
mod models;
mod openapi;
mod responses;
mod routes;

#[derive(Clone, Debug, Deserialize)]
struct Config {
    database_url: String,
    jwt_secret: String,
}

#[derive(Clone, Debug)]
pub struct AppState {
    conn: DbConn,
    config: Config,
}

#[tokio::main]
pub async fn main() {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG")
                .unwrap_or_else(|_| "users_service=debug,tower_http=debug,sqlx=debug".into()),
        ))
        .with(tracing_subscriber::fmt::layer())
        .init();

    let config = envy::from_env::<Config>().unwrap();

    let conn = Database::connect(&config.database_url).await.unwrap();
    let state = AppState { conn, config };

    Migrator::up(&state.conn, None).await.unwrap();

    let app = Router::new()
        .merge(
            Router::new()
                .route("/:id", get(get_by_id))
                .route("/me", get(get_self))
                .route("/login", post(login))
                .route("/search", get(search))
                .with_state(state),
        )
        .merge(ApiDoc::router().with_state(()))
        .layer(TraceLayer::new_for_http())
        .fallback(fallback);

    let addr = SocketAddr::from(([0, 0, 0, 0], 8010));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
