use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};

#[derive(Clone, Debug, PartialEq, Eq, DeriveEntityModel, Deserialize, Serialize)]
#[sea_orm(table_name = "credentials")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub user_id: i32,
    pub password: String,
    pub salt: String,
}

impl Model {
    pub fn verify_password(&self, password_to_verify: &str) -> bool {
        let hash = format!(
            "{:x}",
            Sha256::new()
                .chain_update(password_to_verify)
                .chain_update(&self.salt)
                .finalize()
        );

        self.password == hash
    }
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "super::user::Entity",
        from = "Column::UserId",
        to = "super::user::Column::Id"
    )]
    User,
}

// `Related` trait has to be implemented by hand
impl Related<super::user::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::User.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_verify_password() {
        let model = Model {
            id: 0,
            user_id: 0,
            password: "d1366cf984712a5899da4badde124143ec82fcb1942942950251aadf4957a015"
                .to_string(),
            salt: "test_salt".to_string(),
        };

        assert!(model.verify_password("youshallnotpass"));
    }
}
