use axum::{
    response::{Html, IntoResponse},
    routing::get,
    Json, Router,
};
use utoipa::{
    openapi::{
        self,
        security::{ApiKey, ApiKeyValue, SecurityScheme},
        ServerBuilder,
    },
    Modify, OpenApi,
};

use crate::{
    models::user::Model as User,
    responses::AppError,
    routes::{self, LoginRequest, LoginResponse},
};

const DOCS_TEMPLATE: &str = r#"<!DOCTYPE html><html lang="en"><head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><title>Documentation</title><link rel="stylesheet" href="https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui.css" /></head><body><div id="ui"></div><script src="https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui-bundle.js" crossorigin></script><script>window.onload = () => { window.ui = SwaggerUIBundle({spec: %SPEC%, dom_id: '#ui'}) }</script></body></html>"#;

#[derive(OpenApi, Debug)]
#[openapi(
    paths(routes::get_by_id, routes::get_self, routes::login, routes::search),
    components(schemas(AppError, User, LoginRequest, LoginResponse)),
    modifiers(&SecurityAddon, &InfoAddon, &ServersAddon),
)]
pub struct ApiDoc;

impl ApiDoc {
    pub fn router() -> Router {
        async fn schema_handler() -> impl IntoResponse {
            Json(ApiDoc::openapi())
        }

        async fn docs_handler() -> impl IntoResponse {
            Html(DOCS_TEMPLATE.replace("%SPEC%", &ApiDoc::openapi().to_json().unwrap()))
        }

        Router::new()
            .route("/openapi.json", get(schema_handler))
            .route("/swagger", get(docs_handler))
    }
}

struct InfoAddon;
impl Modify for InfoAddon {
    fn modify(&self, openapi: &mut openapi::OpenApi) {
        openapi.info = openapi::InfoBuilder::new()
            .title("FDL Users Service API")
            .description(Some(
                "The main service for identifying users of the FDL ecosystem.",
            ))
            .version(env!("CARGO_PKG_VERSION"))
            .license(Some(
                openapi::LicenseBuilder::new()
                    .name("GNU General Public License v3.0")
                    .url(Some("https://www.gnu.org/licenses/gpl-3.0-standalone.html"))
                    .build(),
            ))
            .build();
    }
}

struct SecurityAddon;
impl Modify for SecurityAddon {
    fn modify(&self, openapi: &mut openapi::OpenApi) {
        if let Some(components) = openapi.components.as_mut() {
            components.add_security_scheme(
                "api_key",
                SecurityScheme::ApiKey(ApiKey::Header(ApiKeyValue::new("x-token"))),
            )
        }
    }
}

struct ServersAddon;
impl Modify for ServersAddon {
    fn modify(&self, openapi: &mut openapi::OpenApi) {
        openapi.servers = Some(vec![
            ServerBuilder::new()
                .url("https://api.fdl-mc.ru/users/v1")
                .description(Some("Production instance"))
                .build(),
            ServerBuilder::new()
                .url("http://localhost:8010")
                .description(Some("Local development"))
                .build(),
        ]);
    }
}
