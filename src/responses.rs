use serde::Serialize;
use utoipa::ToSchema;

/// Service error data
#[derive(Debug, Serialize, ToSchema)]
pub struct AppError {
    /// Error detail message
    detail: String,
}
impl AppError {
    pub fn new(detail: impl Into<String>) -> Self {
        AppError {
            detail: detail.into(),
        }
    }
}
