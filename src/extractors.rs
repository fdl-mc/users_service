use crate::{claims::Claims, responses::AppError, AppState};
use axum::{
    extract::FromRequestParts,
    http::{request::Parts, StatusCode},
    Json,
};

pub struct AuthClaims(pub Claims);

#[axum::async_trait]
impl FromRequestParts<AppState> for AuthClaims {
    type Rejection = (StatusCode, Json<AppError>);

    async fn from_request_parts(
        parts: &mut Parts,
        state: &AppState,
    ) -> Result<Self, Self::Rejection> {
        let token = parts
            .headers
            .get("x-token")
            .and_then(|t| t.to_str().ok())
            .ok_or((
                StatusCode::UNAUTHORIZED,
                Json(AppError::new("No token provided")),
            ))?;

        Claims::from_jwt(token, &state.config.jwt_secret)
            .map(AuthClaims)
            .map_err(|_| {
                (
                    StatusCode::UNAUTHORIZED,
                    Json(AppError::new("Authentication failed")),
                )
            })
    }
}
