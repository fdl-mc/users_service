FROM rust:slim-bullseye as builder
WORKDIR /build
COPY . .
RUN cargo install --path .

FROM debian:bullseye-slim
WORKDIR /
COPY --from=builder /build/target/release/users_service ./
CMD ["./users_service"]
